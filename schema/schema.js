const graphql = require('graphql');
const {GraphQLObjectType, GraphQLString, GraphQLSchema, GraphQLID, GraphQLInt, GraphQLList, GraphQLBoolean} = graphql;
const Movie = require('../models/Movie');
const Director = require('../models/Director');

const MovieType = new GraphQLObjectType({
    name:'Movie',
    fields:()=>(
        {
            id:{type:GraphQLID},
            name:{type:GraphQLString},
            genre:{type:GraphQLString},
            directorId:{
                type:DirectorType,
                resolve({directorId}, args){
                    return Director.findById(directorId);
                }
            },
            watched:{type:GraphQLBoolean},
            rate:{type:GraphQLInt}
        }
    )
})

const DirectorType = new GraphQLObjectType({
    name:'Director',
    fields:()=>(
    {
        id:{type:GraphQLID},
        name:{type:GraphQLString},
        age:{type:GraphQLInt},
        movies:
            {
                type:new GraphQLList(MovieType),
                resolve({id}, args){
                   return Movie.find({directorId:id});
                }
            }
    })
})

const Mutation = new GraphQLObjectType({
    name:"Mutation",
    fields:{
        addDirector:{
            type:DirectorType,
            args:{
                name:{type:GraphQLString},
                age:{type:GraphQLInt}
            },
            resolve(parent, {name, age}){
               const director = new Director({name, age});
               return director.save();
            }
        },
        addMovie:{
            type:MovieType,
            args:{
                name:{type:GraphQLString},
                genre:{type:GraphQLString},
                directorId:{type:GraphQLID},
                watched:{type:GraphQLBoolean},
                rate:{type:GraphQLInt}
            },
            resolve(parent, {name, genre, directorId, watched, rate}){
                const movie = new Movie({name, genre, directorId, watched, rate});
                return movie.save();
            }
        },
        deleteDirector:{
            type:DirectorType,
            args:{
                id:{type:GraphQLID},
            },
            resolve(parent, {id}){
                return Director.findByIdAndRemove(id);
            }
        },
        deleteMovie:{
            type:MovieType,
            args:{
                id:{type:GraphQLID},
            },
            resolve(parent, {id}){
                return Movie.findByIdAndRemove(id);
            }
        },
        updateDirector:{
            type:DirectorType,
            args:{
                id:{type:GraphQLID},
                name:{type:GraphQLString},
                age:{type:GraphQLInt}
            },
            resolve(parent, {id, name, age}){
                const newDirector = {name, age};
                return Director.findByIdAndUpdate(id, newDirector, {new:true});
            }
        },
        updateMovie:{
            type:MovieType,
            args:{
                id:{type:GraphQLID},
                name:{type:GraphQLString},
                genre:{type:GraphQLString},
                directorId:{type:GraphQLID},
                watched:{type:GraphQLBoolean},
                rate:{type:GraphQLInt}
            },
            resolve(parent, {id, name, genre, directorId, watched, rate}){
                const newMovie = {name, genre, directorId, watched, rate};
                return Movie.findByIdAndUpdate(id, newMovie);
            }
        }

    }
})

const Query = new GraphQLObjectType({
    name:'Query',
    fields:{
        movie:{
            type:MovieType,
            args:{id:{type:GraphQLID}},
            resolve(parent, {id}){
                return Movie.findById(id);
            }
        },
        director:{
            type:DirectorType,
            args:{id:{type:GraphQLID}},
            resolve(parent, {id}){
                return Director.findById(id);
            }
        },
        movies:{
            type:new GraphQLList(MovieType),
            resolve(){
                return Movie.find({});
            }
        },
        directors:{
            type:new GraphQLList(DirectorType),
            resolve(){
                return Director.find({});
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query:Query,
    mutation:Mutation
});
