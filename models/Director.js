const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const DirectorSchema = new Schema({
    name:{type: String},
    age:{type:Number},
});


const Director = mongoose.model('Director', DirectorSchema);
module.exports = Director;


