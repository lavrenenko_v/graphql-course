const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const MovieSchema = new Schema({
    name:{type:String},
    genre:{type:String},
    directorId:{type:String},
    watched:{type:Boolean},
    rate:{type:Number},
});


const Movie = mongoose.model('Movie', MovieSchema);
module.exports = Movie;



