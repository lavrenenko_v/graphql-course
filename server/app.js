const express = require('express');
const app = express();
const {graphqlHTTP} = require('express-graphql');
const schema = require('../schema/schema');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const connectDB = require('../config/db');
const path = require("path");
const cors = require('cors');

// Load config
dotenv.config({path:path.resolve(__dirname,'../config/config.env')});
connectDB();

const PORT = 3005;

//Enable CORS
app.use(cors());

app.use('/graphql', graphqlHTTP((req, res) => ({
    schema,
    graphiql: true,
})));

app.listen(PORT, err=>{
    err?console.log(err):console.log(`Server is running on port ${PORT}`)})
