import {gql} from 'apollo-boost';

export const moviesQuery = gql`
  query{
    movies {
      id
      name
      genre
      directorId{
        id      
        name
      }
      watched
      rate
    }
  }
`;
