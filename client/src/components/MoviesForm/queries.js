import {gql} from 'apollo-boost';


export const directorsQuery = gql`
    query{
        directors{
            id
            name
        }
    }
`;
